summary: Rebuild kernel, install it, reboot, and check if we're running the correct
    kernel
description: |
    Rebuild kernel, install it, reboot, and check if we're running the correct
    kernel. Tailored specificaly for binutils buildroot testing process.

    Author: Milos Prchlik <mprchlik@redhat.com>

    Based on gcc/Sanity/rebuild-kernel by:
    Author: Michal Nowak <mnowak@redhat.com>
    Author: Marek Polacek <polacek@redhat.com>

    Using MACHINE_SET__STRONG host-filter for this test, by setting the "hardware"
    section in TCMS properly. That has several effects:

      - kernel rebuild is very resoruce-intensive task, and having more powerful
        boxes for it is simply good,
      - this task will get its own boxes, not clobbered by additional kernel packages
        that are usually installed by other tasks in the same run. E.g.kernel-debuginfo,
        when installed, will conflict with freshly build kernel packages. This should
        workaround such situations,
      - this tasks reboots its boxes - should such reboot break something, don't ruin
        the whole run by it, right?
contact:
- Milos Prchlik <mprchlik@redhat.com>
component:
- binutils
test: ./runtest.sh
framework: beakerlib
recommend:
- binutils
- gcc
- yum-utils
- rng-tools
- rpm-build
- newt-devel
- python-devel
- perl-ExtUtils-Embed
- unifdef
- elfutils-libelf-devel
- elfutils-devel
- pciutils-devel
- wget
- hmaccalc
- binutils-devel
- glibc-static
- texinfo
- gdb
- ecj
- gcc-java
- ppl-devel
- cloog-ppl-devel
- graphviz
- gmp-devel
- mpfr-devel
- xmlto
- asciidoc
- net-tools
- pesign
duration: 20h
extra-summary: /tools/binutils/Sanity/rebuild-kernel-and-reboot
extra-task: /tools/binutils/Sanity/rebuild-kernel-and-reboot
